import axios from "axios";
const baseURL = process.env.REACT_APP_BACK_END_API;
const jwt = localStorage.getItem("jwt");

const app = axios.create({
  baseURL,
  withCredentials: true,
  headers: {
    "Authorization": `Bearer ${jwt}`,
    "Content-Type": "application/json",
  },
});

export default app;
