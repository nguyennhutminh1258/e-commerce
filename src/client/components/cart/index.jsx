import { useNavigate } from "react-router-dom";
import CartItem from "../cart-item";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getCart } from "../../../store/slice/cartSlice";

const Cart = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const cart = useSelector((state) => state.cart.cart);
  const cartItems = useSelector((state) => state.cart.cartItems);

  const handleCheckout = () => {
    navigate("/checkout?step=2");
  };

  useEffect(() => {
    dispatch(getCart());
  }, []);

  return (
    <div className="lg:grid grid-cols-3 lg:px-16 relative">
      <div className="col-span-2">
        {cartItems?.map((item) => (
          <CartItem item={item} key={item._id} />
        ))}
      </div>

      <div className="px-5 sticky top-0 h-[100vh] mt-5 lg:mt-0">
        <div className="border">
          <p className="uppercase font-bold opacity-60 pb-4">Price details</p>

          <hr />

          <div className="space-y-3 font-semibold">
            <div className="flex justify-between pt-3 text-black">
              <span>Price</span>
              <span>$ {cart?.totalPrice}</span>
            </div>

            <div className="flex justify-between pt-3">
              <span>Discount</span>
              <span className="text-red-600">$ {cart?.discount}</span>
            </div>

            <div className="flex justify-between pt-3">
              <span>Deliver Charges</span>
              <span className="text-red-600">Free</span>
            </div>

            <hr />

            <div className="flex justify-between pt-3 font-bold">
              <span>Total Amount</span>
              <span className="text-red-700">
                $ {cart?.totalDiscountedPrice}
              </span>
            </div>
          </div>

          <button
            onClick={handleCheckout}
            className="mt-10 flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-8 py-3 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
          >
            Check out
          </button>
        </div>
      </div>
    </div>
  );
};

export default Cart;
