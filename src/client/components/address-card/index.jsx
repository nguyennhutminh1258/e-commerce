const AddressCard = ({ address }) => {
  return (
    <div className="space-y-3">
      <p className="font-semibold">
        {address?.firstName + " " + address?.lastName}
      </p>
      <p>{address.address + " " + address?.city + " " + address?.province}</p>
      <div className="space-y-3">
        <p className="font-semibold">Phone Number</p>
        <p>{address?.phoneNumber}</p>
      </div>
    </div>
  );
};

export default AddressCard;
