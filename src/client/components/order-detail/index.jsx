import { Box, Grid } from "@mui/material";
import AddressCard from "../address-card";
import OrderTraker from "../order-traker";
import { deepPurple } from "@mui/material/colors";
import StarIcon from "@mui/icons-material/Star";

const OrderDetail = () => {
  return (
    <div className="px-5 lg:px-20">
      <div>
        <h1 className="font-bold text-lg py-5">Delivery Address</h1>
        <AddressCard />
      </div>

      <div className="py-20">
        <OrderTraker activeStep={2} />
      </div>

      <Grid className="space-y-5" container>
        {[1, 1, 1, 1].map((item) => (
          <Grid
            item
            container
            className="shadow-xl rounded-md p-5 border"
            sx={{ alignItems: "center", justifyContent: "space-between" }}
          >
            <Grid item xs={6}>
              <div className="flex items-center space-x-5">
                <img
                  className="w-[6rem] h-[6rem] object-cover object-top"
                  src="https://rukminim1.flixcart.com/image/612/612/xif0q/shirt/x/m/n/40-men-regular-slim-fit-solid-button-down-collar-formal-shirt-original-imagf4mn9zg7qrmf-bb.jpeg?q=70"
                  alt=""
                />

                <div className="space-y-2 ml-5">
                  <p className="font-semibold">
                    Men Slim Fit Solid Spread Collar Formal Shirt
                  </p>
                  <p className="space-x-5 opacity-50 text-xs font-semibold">
                    <span>Color: pink</span> <span>Size: M</span>
                  </p>
                  <p>Brand: MILDIN</p>
                  <p>$ 1999</p>
                </div>
              </div>
            </Grid>

            <Grid item>
              <Box sx={{ color: deepPurple[500] }}>
                <StarIcon sx={{ fontSize: "2rem" }} className="px-2" />
                <span>Rate & Review Product</span>
              </Box>
            </Grid>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default OrderDetail;
