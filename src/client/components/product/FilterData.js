export const color = ["white", "black", "red", "Pink", "Green", "Yellow"];

export const filters = [
  {
    id: "color",
    name: "Color",
    options: [
      {
        value: "white",
        label: "White",
      },
      {
        value: "blue",
        label: "Blue",
      },
      {
        value: "green",
        label: "Green",
      },
      {
        value: "purple",
        label: "Purple",
      },
      {
        value: "yellow",
        label: "Yellow",
      },
    ],
  },

  {
    id: "size",
    name: "Size",
    options: [
      {
        value: "S",
        label: "S",
      },
      {
        value: "M",
        label: "M",
      },
      {
        value: "L",
        label: "L",
      },
    ],
  },
];

export const singleFilter = [
  {
    id: "price",
    name: "Price",
    options: [
      {
        value: "139-159",
        label: "$139 to $159",
      },
      {
        value: "399-999",
        label: "$399 to $999",
      },
      {
        value: "999-1999",
        label: "$999 to $1999",
      },
      {
        value: "1999-2999",
        label: "$1999 to $2999",
      },
    ],
  },
  {
    id: "discount",
    name: "Discount",
    options: [
      {
        value: "10",
        label: "10% And Above",
      },
      {
        value: "20",
        label: "20% And Above",
      },
      {
        value: "30",
        label: "30% And Above",
      },
      {
        value: "40",
        label: "40% And Above",
      },
    ],
  },
  {
    id: "stock",
    name: "Availability",
    options: [
      {
        value: "in_stock",
        label: "In Stock",
      },
      {
        value: "out_of_stock",
        label: "Out Of Stock",
      },
    ],
  },
];
