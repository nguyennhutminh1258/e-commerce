import { Box, Button, Grid, TextField } from "@mui/material";
import AddressCard from "../address-card";
import { useDispatch } from "react-redux";
import { createOrder } from "../../../store/slice/orderSlice";
import { useNavigate } from "react-router-dom";

const DeliveryAddressForm = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleSubmit = (e) => {
    e.preventDefault();

    const data = new FormData(e.currentTarget);

    const address = {
      firstName: data.get("firstName"),
      lastName: data.get("lastName"),
      address: data.get("address"),
      city: data.get("city"),
      province: data.get("province"),
      identityNumber: data.get("identityNumber"),
      phoneNumber: data.get("phoneNumber"),
    };

    const orderAddress = {address, navigate}

    dispatch(createOrder(orderAddress))
  };

  return (
    <Grid container spacing={4}>
      <Grid
        xs={12}
        lg={5}
        className="border rounded-e-md shadow-md h-[30rem] overflow-y-scroll"
      >
        <div className="p-5 py-7 border-b cursor-pointer">
          <AddressCard />
          <Button variant="contained" sx={{ mt: 2 }}>
            Deliver Here
          </Button>
        </div>
      </Grid>

      <Grid item xs={12} lg={7}>
        <Box className="border rounded-s-md shadow-md p-5">
          <form onSubmit={(e) => handleSubmit(e)}>
            <Grid container spacing={3}>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="firstName"
                  name="firstName"
                  label="First Name"
                  fullWidth
                  autoComplete="given-name"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="lastName"
                  name="lastName"
                  label="Last Name"
                  fullWidth
                  autoComplete="given-name"
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  required
                  id="address"
                  name="address"
                  label="Address"
                  fullWidth
                  multiline
                  rows={5}
                  autoComplete="given-name"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="city"
                  name="city"
                  label="City"
                  fullWidth
                  autoComplete="given-name"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="province"
                  name="province"
                  label="Province"
                  fullWidth
                  autoComplete="given-name"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="identityNumber"
                  name="identityNumber"
                  label="Identity Number"
                  fullWidth
                  autoComplete="given-name"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="phoneNumber"
                  name="phoneNumber"
                  label="Phone Number"
                  fullWidth
                  autoComplete="given-name"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <Button variant="contained" sx={{ mt: 2 }} type="submit">
                  Deliver Here
                </Button>
              </Grid>
            </Grid>
          </form>
        </Box>
      </Grid>
    </Grid>
  );
};

export default DeliveryAddressForm;
