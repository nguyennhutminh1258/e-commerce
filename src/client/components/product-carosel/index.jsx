import AliceCarousel from "react-alice-carousel";
import { Button } from "@mui/material";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";
import { useState } from "react";
import ProductCaroselCard from "../product-carosel-card";

const ProductCarosel = ({ data, sectionName }) => {
  const [activeIndex, setActiveIndex] = useState(0);

  const responsive = {
    0: { items: 1 },
    720: { items: 3 },
    1024: { items: 5 },
  };

  const handleClickPrev = () => {
    setActiveIndex((prev) => prev - 1);
  };

  const handleClickNext = () => {
    setActiveIndex((prev) => prev + 1);
  };

  const syncActiveIndex = ({ item }) => {
    setActiveIndex(item);
  };

  const items = data
    .slice(0, 10)
    .map((item) => <ProductCaroselCard product={item} />);

  return (
    <div className="relative border">
      <h2 className="text-2xl font-extrabold text-gray-800 py-5 px-10">
        {sectionName}
      </h2>
      <div className="relative p-5">
        <AliceCarousel
          items={items}
          responsive={responsive}
          disableButtonsControls
          disableDotsControls
          onSlideChange={syncActiveIndex}
          activeIndex={activeIndex}
        />
        {activeIndex !== 0 && (
          <Button
            variant="contained"
            className="z-50"
            onClick={handleClickPrev}
            sx={{ position: "absolute", top: "8rem", left: "0rem" }}
          >
            <KeyboardArrowLeftIcon />
          </Button>
        )}

        {activeIndex !== items.length - 5 && (
          <Button
            variant="contained"
            className="z-50"
            onClick={handleClickNext}
            sx={{ position: "absolute", top: "8rem", right: "0rem" }}
          >
            <KeyboardArrowRightIcon />
          </Button>
        )}
      </div>
    </div>
  );
};

export default ProductCarosel;
