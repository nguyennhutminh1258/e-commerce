export const navigation = {
  categories: [
    {
      id: "women",
      name: "Women",
      sections: [
        {
          id: "clothing",
          name: "Clothing",
          items: [
            { name: "Tops", id: "top" },
            { name: "Dresses", id: "women_dress" },
            { name: "Women Jeans", id: "women_jeans" },
            { name: "Sweaters", id: "sweaters" },
            { name: "T-Shirts", id: "t-shirt" },
            { name: "Jackets", id: "jackets" },
          ],
        },
        {
          id: "accessories",
          name: "Accessories",
          items: [
            { name: "Watches", id: "watches" },
            { name: "Wallets", id: "wallets" },
            { name: "Bags", id: "bags" },
            { name: "Sunglasses", id: "sunglasses" },
            { name: "Hats", id: "hats" },
            { name: "Belts", id: "belts" },
          ],
        },
        {
          id: "brands",
          name: "Brands",
          items: [
            { name: "Full Nelson", id: "full_nelson" },
            { name: "My Way", id: "my_way" },
            { name: "Re-Arranged", id: "re-arranged" },
            { name: "Counterfeit", id: "counterfeit" },
          ],
        },
      ],
    },
    {
      id: "men",
      name: "Men",
      featured: [
        {
          name: "New Arrivals",
          id: "",
          imageSrc:
            "https://tailwindui.com/img/ecommerce-images/product-page-04-detail-product-shot-01.jpg",
          imageAlt:
            "Drawstring top with elastic loop closure and textured interior padding.",
        },
        {
          name: "Artwork Tees",
          id: "",
          imageSrc:
            "https://tailwindui.com/img/ecommerce-images/category-page-02-image-card-06.jpg",
          imageAlt:
            "Three shirts in gray, white, and blue arranged on table with same line drawing of hands and shapes overlapping on front of shirt.",
        },
      ],
      sections: [
        {
          id: "clothing",
          name: "Clothing",
          items: [
            { name: "Tops", id: "men_tops" },
            { name: "Pants", id: "pants" },
            { name: "Sweaters", id: "sweaters" },
            { name: "T-Shirts", id: "t-shirts" },
            { name: "Jackets", id: "jackets" },
          ],
        },
        {
          id: "accessories",
          name: "Accessories",
          items: [
            { name: "Watches", id: "watches" },
            { name: "Wallets", id: "wallets" },
            { name: "Bags", id: "bags" },
            { name: "Sunglasses", id: "sunglasses" },
            { name: "Hats", id: "hats" },
            { name: "Belts", id: "belts" },
          ],
        },
        {
          id: "brands",
          name: "Brands",
          items: [
            { name: "Full Nelson", id: "full_nelson" },
            { name: "My Way", id: "my_way" },
            { name: "Re-Arranged", id: "re-arranged" },
            { name: "Counterfeit", id: "counterfeit" },
          ],
        },
      ],
    },
  ],
  // pages: [
  //   { name: 'Company', href: '#' },
  //   { name: 'Stores', href: '#' },
  // ],
};
