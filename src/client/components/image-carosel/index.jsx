import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/alice-carousel.css";
import { imageCaroselData } from "./ImageCaroselData";

const ImageCarorel = () => {
  const items = imageCaroselData.map((item) => (
    <img
      src={item.image}
      className="h-[700px] w-full object-cover"
      role="presentation"
      alt=""
    />
  ));

  return (
    <AliceCarousel
      items={items}
      disableButtonsControls
      autoPlay
      autoPlayInterval={2000}
      infinite
    />
  );
};

export default ImageCarorel;
