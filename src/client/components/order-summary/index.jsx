import { useDispatch, useSelector } from "react-redux";
import AddressCard from "../address-card";
import CartItem from "../cart-item";
import { useEffect } from "react";
import { getOrderById } from "../../../store/slice/orderSlice";
import { useLocation } from "react-router-dom";

const OrderSummary = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const orders = useSelector((state) => state.order.orders);
  const order = useSelector((state) => state.order.order);

  const searchParams = new URLSearchParams(location.search);
  const orderId = searchParams.get("order_id");

  useEffect(() => {
    dispatch(getOrderById(orderId));
  }, [orderId]);

  return (
    <div>
      <div className="p-5 shadow-lg rounded-s-md border">
        <AddressCard address={orders?.shippingAddress} />
      </div>

      <div className="lg:grid grid-cols-3 relative">
        <div className="col-span-2">
          {orders?.map((item) => (
            <CartItem item={item} key={item._id} />
          ))}
        </div>

        <div className="px-5 sticky top-0 h-[100vh] mt-5 lg:mt-0">
          <div className="border">
            <p className="uppercase font-bold opacity-60 pb-4">Price details</p>

            <hr />

            <div className="space-y-3 font-semibold">
              <div className="flex justify-between pt-3 text-black">
                <span>Price</span>
                <span>$ {order?.totalPrice}</span>
              </div>

              <div className="flex justify-between pt-3">
                <span>Discount</span>
                <span className="text-red-600">$ {order?.discount}</span>
              </div>

              <div className="flex justify-between pt-3">
                <span>Deliver Charges</span>
                <span className="text-red-600">Free</span>
              </div>

              <hr />

              <div className="flex justify-between pt-3 font-bold">
                <span>Total Amount</span>
                <span className="text-red-700">$ {order?.totalDiscountedPrice}</span>
              </div>
            </div>

            <button
              type="submit"
              className="mt-10 flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 px-8 py-3 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
            >
              Check out
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OrderSummary;
