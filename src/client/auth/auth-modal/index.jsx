import { Box, Modal } from "@mui/material";
import RegisterForm from "../register-form";
import { useLocation } from "react-router-dom";
import LoginForm from "../login-form";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 500,
  bgcolor: "background.paper",
  outline: "none",
  boxShadow: 24,
  p: 4,
};

const AuthModal = ({ open, handleClose }) => {
  const location = useLocation();

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          {location.pathname === "/register" ? <RegisterForm /> : <LoginForm />}
        </Box>
      </Modal>
    </div>
  );
};

export default AuthModal;
