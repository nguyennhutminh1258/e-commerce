import React from "react";
import ImageCarorel from "../../components/image-carosel";
import ProductCarosel from "../../components/product-carosel";
import { men_shirt } from "../../../data/men_shirt";

const HomePage = () => {
  return (
    <div>
      <ImageCarorel />

      <div className="space-y-10 py-10 flex flex-col justify-center px-5 lg:px-10">
        <ProductCarosel data={men_shirt} sectionName={"Men's Shirt"}/>
      </div>
    </div>
  );
};

export default HomePage;
