import { Route, Routes } from "react-router-dom";
import HomePage from "../client/pages/home-page";
import Cart from "../client/components/cart";
import Navbar from "../client/components/navbar";
import Footer from "../client/components/footer";
import Product from "../client/components/product";
import ProductDetail from "../client/components/product-detail";
import Checkout from "../client/components/checkout";
import Order from "../client/components/order";
import OrderDetail from "../client/components/order-detail";

const ClientRouters = () => {
  return (
    <div>
      <div>
        <Navbar />
      </div>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<HomePage />} />
        <Route path="/register" element={<HomePage />} />
        <Route path="/cart" element={<Cart />} />
        <Route path="/:labelOne/:labelTwo/:labelThree" element={<Product />} />
        <Route path="/product/:productId" element={<ProductDetail />} />
        <Route path="/checkout" element={<Checkout />} />
        <Route path="/account/order" element={<Order />} />
        <Route path="/account/order/:orderId" element={<OrderDetail />} />
      </Routes>
      <div>
        <Footer />
      </div>
    </div>
  );
};

export default ClientRouters;
