import "./App.css";
import { Route, Routes } from "react-router-dom";
import ClientRouters from "./routers/ClientRouters";
import AdminRouters from "./routers/AdminRouters";

function App() {
  return (
    <div>
      <Routes>
        <Route path="/*" element={<ClientRouters />} />
        <Route path="/admin/*" element={<AdminRouters />} />
      </Routes>
    </div>
  );
}

export default App;
