import { combineReducers, configureStore } from "@reduxjs/toolkit";
import authSlice from "./slice/authSlice";
import productSlice from "./slice/productSlice";
import cartSlice from "./slice/cartSlice";

const rootReducer = combineReducers({
  auth: authSlice,
  product: productSlice,
  cart: cartSlice,
});

const store = configureStore({ reducer: rootReducer });

export default store;
