import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import app from "../../config/apiConfig";

const productSlice = createSlice({
  name: "product",
  initialState: {
    products: [],
    product: null,
    loading: false,
  },
  extraReducers: (builder) => {
    builder
      .addCase(findProduct.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(findProductById.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(findProduct.fulfilled, (state, action) => {
        state.loading = false;
        state.products = action.payload;
      })
      .addCase(findProductById.fulfilled, (state, action) => {
        state.loading = false;
        state.product = action.payload;
      });
  },
});

export const findProduct = createAsyncThunk(
  "product/findProduct",
  async (reqData) => {
    let {
      category,
      color,
      sizes,
      minPrice,
      maxPrice,
      minDiscount,
      sort,
      stock,
      pageNumber,
      pageSize,
    } = reqData;

    const { data } = await app.get(
      `/api/product/color=${color}&sizes=${sizes}&minPrice=${minPrice}&maxPrice=${maxPrice}&minDiscount=${minDiscount}&category=${category}&stock=${stock}&sort=${sort}&pageNumber=${pageNumber}&pageSize=${pageSize}`
    );

    return data;
  }
);

export const findProductById = createAsyncThunk(
  "product/findProductById",
  async (reqData) => {
    let { productId } = reqData;

    const { data } = await app.get(`/api/product/id/${productId}`);

    return data;
  }
);

export const createProduct = createAsyncThunk(
  "product/createProduct",
  async (product) => {
    const { data } = await app.post(`/api/admin/product`, product.data);

    return data;
  }
);

export const deleteProduct = createAsyncThunk(
  "product/deleteProduct",
  async (productId) => {
    const { data } = await app.delete(`/api/admin/product/${productId}`);

    return data;
  }
);


export default productSlice.reducer;
