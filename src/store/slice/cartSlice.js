import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import app from "../../config/apiConfig";

const cartSlice = createSlice({
  name: "cart",
  initialState: {
    cart: null,
    loading: false,
    cartItems: [],
  },
  extraReducers: (builder) => {
    builder
      .addCase(addItemToCart.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(getCart.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(removeCartItem.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(updateCartItem.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(addItemToCart.fulfilled, (state, action) => {
        state.loading = false;
        state.cartItems.push(action.payload);
      })
      .addCase(getCart.fulfilled, (state, action) => {
        state.loading = false;
        state.cart = action.payload;
        state.cartItems = action.payload.cartItems;
      })
      .addCase(removeCartItem.fulfilled, (state, action) => {
        state.loading = false;
        state.cartItems = state.cartItems.filter(
          (item) => item.id !== action.payload
        );
      })
      .addCase(updateCartItem.fulfilled, (state, action) => {
        state.loading = false;
        state.cartItems = action.payload
      });
  },
});

export const addItemToCart = createAsyncThunk(
  "cart/addItemToCart",
  async (reqData) => {
    const { data } = await app.put("/api/cart/add", reqData);

    return data;
  }
);

export const getCart = createAsyncThunk("cart/getCart", async () => {
  const { data } = await app.get(`/api/cart/`);

  return data;
});

export const removeCartItem = createAsyncThunk(
  "cart/removeCartItem",
  async (cartItemId) => {
    const { data } = await app.delete(`/api/cart_items/${cartItemId}`);

    return cartItemId;
  }
);

export const updateCartItem = createAsyncThunk(
  "cart/updateCartItem",
  async (reqData) => {
    const { data } = await app.put(
      `/api/cart_items/${reqData.cartItemId}`,
      reqData.data
    );

    return data;
  }
);

export default cartSlice.reducer;
