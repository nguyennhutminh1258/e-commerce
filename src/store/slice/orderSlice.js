import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import app from "../../config/apiConfig";

const orderSlice = createSlice({
  name: "order",
  initialState: {
    orders: [],
    order: null,
    loading: false,
  },
  extraReducers: (builder) => {
    builder
      .addCase(createOrder.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(getOrderById.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(createOrder.fulfilled, (state, action) => {
        state.loading = false;
        state.order = action.payload;
      })
      .addCase(getOrderById.fulfilled, (state, action) => {
        state.loading = false;
        state.order = action.payload;
      });
  },
});

export const createOrder = createAsyncThunk(
  "order/createOrder",
  async (reqData) => {

    const { data } = await app.post(`/api/order/`, reqData.address);
    if (data._id) {
      reqData.navigate({search: `step=3&order_id=${data._id}`})
    }

    return data;
  }
);

export const getOrderById = createAsyncThunk(
  "order/getOrderById",
  async (reqData) => {
    let { productId } = reqData;

    const { data } = await app.get(`/api/product/id/${productId}`);

    return data;
  }
);

export default orderSlice.reducer;
