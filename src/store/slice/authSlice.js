import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import app from "../../config/apiConfig";

const authSlice = createSlice({
  name: "auth",
  initialState: {
    user: null,
    isLoading: false,
    jwt: null,
  },
  reducers: {
    logout(state, action) {
      state.user = null;
      state.jwt = null;
      localStorage.clear();
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(register.pending, (state, action) => {
        state.isLoading = true;
      })
      .addCase(register.fulfilled, (state, action) => {
        state.isLoading = false;
        state.user = action.payload;
      })
      .addCase(login.pending, (state, action) => {
        state.isLoading = true;
      })
      .addCase(login.fulfilled, (state, action) => {
        state.isLoading = false;
        state.jwt = action.payload;
      })
      .addCase(getUser.pending, (state, action) => {
        state.isLoading = true;
      })
      .addCase(getUser.fulfilled, (state, action) => {
        state.isLoading = false;
        state.user = action.payload;
      });
  },
});

export const { logout } = authSlice.actions;

export const register = createAsyncThunk("auth/register", async (userData) => {
  const response = await app.post("auth/signup", userData);
  const user = response.data;

  if (user.jwt) {
    localStorage.setItem("jwt", user.jwt);
  }

  return user.jwt;
});

export const login = createAsyncThunk("auth/login", async (userData) => {
  const response = await app.post("auth/signin", userData);
  const user = response.data;

  if (user.jwt) {
    localStorage.setItem("jwt", user.jwt);
  }

  return user.jwt;
});

export const getUser = createAsyncThunk("auth/getUser", async (userData) => {
  const token = localStorage.getItem("jwt");
  const response = await app.get("api/user/profile", {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const user = response.data;

  if (user.jwt) {
    localStorage.setItem("jwt", user.jwt);
  }

  return user;
});

export default authSlice.reducer;
