import {
  Box,
  Button,
  FilledInput,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  TextField,
} from "@mui/material";

const sizes = [
  {
    value: "s",
    label: "S",
  },
  {
    value: "m",
    label: "M",
  },
  {
    value: "l",
    label: "L",
  },
  {
    value: "xl",
    label: "XL",
  },
];

const colors = [
  {
    value: "yellow",
    label: "Yellow",
  },
  {
    value: "red",
    label: "Red",
  },
  {
    value: "white",
    label: "white",
  },
  {
    value: "blue",
    label: "Blue",
  },
];

const topLevelCategories = [
  {
    value: "women",
    label: "Women",
  },
  {
    value: "men",
    label: "Men",
  },
];

const secondLevelCategories = [
  {
    value: "clothing",
    label: "Clothing",
  },
  {
    value: "accessories",
    label: "Accessories",
  },
];

const thirdLevelCategories = [
  {
    value: "tops",
    label: "Tops",
  },
  {
    value: "dresses",
    label: "Dresses",
  },
  {
    value: "sweaters",
    label: "Sweaters",
  },
  {
    value: "t-shirts",
    label: "T-Shirts",
  },
  {
    value: "jackets",
    label: "Jackets",
  },
];

const CreateProduct = () => {
  const handleSubmit = (e) => {
    e.preventDefault();

    const data = new FormData(e.currentTarget);

    const address = {
      firstName: data.get("firstName"),
      lastName: data.get("lastName"),
      address: data.get("address"),
      city: data.get("city"),
      province: data.get("province"),
      identityNumber: data.get("identityNumber"),
      phoneNumber: data.get("phoneNumber"),
    };
    console.log("Submit", address);
  };

  return (
    <Grid container spacing={4}>
      <Grid item>
        <Box className="border rounded-s-md shadow-md p-5">
          <form onSubmit={(e) => handleSubmit(e)}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField
                  required
                  id="imageUrl"
                  name="imageUrl"
                  label="Image Url"
                  fullWidth
                  autoComplete="imageUrl"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="title"
                  name="title"
                  label="Title"
                  fullWidth
                  autoComplete="title"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="color"
                  name="color"
                  select
                  label="Color"
                  fullWidth
                >
                  {colors.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>

              <Grid item xs={12} sm={4}>
                <FormControl fullWidth variant="filled">
                  <InputLabel htmlFor="price">Price</InputLabel>
                  <FilledInput
                    required
                    id="price"
                    type="number"
                    name="price"
                    label="Price"
                    startAdornment={
                      <InputAdornment position="start">$</InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={4}>
                <FormControl fullWidth variant="filled">
                  <InputLabel htmlFor="discountedPrice">
                    Discounted Price
                  </InputLabel>
                  <FilledInput
                    required
                    id="discountedPrice"
                    type="number"
                    name="discountedPrice"
                    label="Discounted Price"
                    startAdornment={
                      <InputAdornment position="start">$</InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={4}>
                <FormControl fullWidth variant="filled">
                  <InputLabel htmlFor="price">Discount Persent</InputLabel>
                  <FilledInput
                    required
                    id="discountPersent"
                    type="number"
                    name="discountPersent"
                    label="Discount Persent"
                    startAdornment={
                      <InputAdornment position="start">%</InputAdornment>
                    }
                  />
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={6}>
                <FormControl fullWidth variant="filled">
                  <InputLabel htmlFor="discountedPrice">Quantity</InputLabel>
                  <FilledInput
                    required
                    id="quantity"
                    type="number"
                    name="quantity"
                    label="Quantity"
                  />
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="brand"
                  name="brand"
                  label="Brand"
                  fullWidth
                  autoComplete="brand"
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  required
                  id="description"
                  name="description"
                  label="Description"
                  fullWidth
                  multiline
                  rows={5}
                  autoComplete="description"
                />
              </Grid>

              <Grid item xs={12} sm={4}>
                <TextField
                  required
                  id="topLevelCategory"
                  name="topLevelCategory"
                  select
                  label="Top Level Category"
                  fullWidth
                >
                  {topLevelCategories.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>

              <Grid item xs={12} sm={4}>
                <TextField
                  required
                  id="secondLavelCategory"
                  name="secondLavelCategory"
                  select
                  label="Second Level Category"
                  fullWidth
                >
                  {secondLevelCategories.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>

              <Grid item xs={12} sm={4}>
                <TextField
                  required
                  id="thirdLevelCategories"
                  name="thirdLevelCategories"
                  select
                  label="Third Level Category"
                  fullWidth
                >
                  {thirdLevelCategories.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                  ))}
                </TextField>
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="size-s"
                  name="size-s"
                  label="Size name"
                  defaultValue="S"
                  InputProps={{
                    readOnly: true,
                  }}
                  fullWidth
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <FormControl fullWidth variant="filled">
                  <InputLabel htmlFor="discountedPrice">Quantity</InputLabel>
                  <FilledInput
                    required
                    id="quantity"
                    type="number"
                    name="quantity"
                    label="Quantity"
                  />
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="size-m"
                  name="size-m"
                  label="Size name"
                  defaultValue="M"
                  InputProps={{
                    readOnly: true,
                  }}
                  fullWidth
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <FormControl fullWidth variant="filled">
                  <InputLabel htmlFor="discountedPrice">Quantity</InputLabel>
                  <FilledInput
                    required
                    id="quantity"
                    type="number"
                    name="quantity"
                    label="Quantity"
                  />
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  id="size-l"
                  name="size-l"
                  label="Size name"
                  defaultValue="L"
                  InputProps={{
                    readOnly: true,
                  }}
                  fullWidth
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <FormControl fullWidth variant="filled">
                  <InputLabel htmlFor="discountedPrice">Quantity</InputLabel>
                  <FilledInput
                    required
                    id="quantity"
                    type="number"
                    name="quantity"
                    label="Quantity"
                  />
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={6}>
                <Button variant="contained" sx={{ mt: 2 }} type="submit">
                  Add Product
                </Button>
              </Grid>
            </Grid>
          </form>
        </Box>
      </Grid>
    </Grid>
  );
};

export default CreateProduct;
